#include <stdlib.h>
#include <avr/io.h>
#include "char_lcd.h"
#include "main.h"

void main(void) {
  DDRB |= (0x01 | 0x02 | 0x04 | 0x08 | 0x10 | 0x20);

  sreg displayData = {&PORTB, 0x01, 0x02, 0x04};
  sreg_init(&displayData);

  display charDisp = {&PORTB, &displayData, 0x08, 0x10, 0x20};
  // cursor inc., disable display shift
  lcd_init(&charDisp, 0x19, 0x02, 0x07); //display on, cursor on, cursor blink

  char f_t_l[8] = {
    0x00,
    0x00,
    0x00,
    0x1b,
    0x14,
    0x0b,
    0x12,
    0x11
  };

  char f_t_r[8] = {
    0x00,
    0x00,
    0x00,
    0x1b,
    0x05,
    0x06,
    0x09,
    0x11
  };

  char f_b_r[8] = {
    0x11,
    0x09,
    0x06,
    0x05,
    0x1b,
    0x00,
    0x00,
    0x00
  };

  char f_b_l[8] = {
    0x11,
    0x12,
    0x0b,
    0x14,
    0x1b,
    0x00,
    0x00,
    0x00,
  };

  lcd_save_char(&charDisp, 0, f_t_l);
  lcd_save_char(&charDisp, 1, f_t_r);
  lcd_save_char(&charDisp, 2, f_b_l);
  lcd_save_char(&charDisp, 3, f_b_r);

  lcd_write_cmd(&charDisp, 0x80);
  lcd_write_data(&charDisp, 0x00);
  lcd_write_data(&charDisp, 0x01);
  lcd_write_cmd(&charDisp, 0xC0);
  lcd_write_data(&charDisp, 0x02);
  lcd_write_data(&charDisp, 0x03);

  lcd_write_cmd(&charDisp, 0x82);
}

