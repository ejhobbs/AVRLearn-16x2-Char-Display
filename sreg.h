#ifndef SREG_LIB
#define SREG_LIB
typedef volatile struct sreg {
  volatile char *port;
  char in;
  char out_en;
  char clk;
} sreg, *Psreg;

void sreg_init(Psreg);

void sreg_push(char, Psreg);

void sreg_adv_clk(Psreg);
#endif
