#ifndef SREG_LIB
#include "./sreg.h"
#endif

#ifndef CHAR_LCD_LIB
#define CHAR_LCD_LIB
typedef volatile struct display {
  volatile char *port;
  Psreg data;
  int rs;
  int rw;
  int en;
} display, *Pdisplay;

void lcd_save_char(Pdisplay, int, char*);

void lcd_write_left(Pdisplay, int, char*);

void lcd_write_center(Pdisplay, int, char*);

void lcd_write_offset(Pdisplay, int, int, char*);

void lcd_write_string(Pdisplay, char*);

void lcd_write_data(Pdisplay, char);

void lcd_write_cmd(Pdisplay, char);

void lcd_init(Pdisplay, char, char, char);

void lcd_pulse_clk(Pdisplay);
#endif

#ifndef CHAR_LCD_LIB_CMDS
#define CHAR_LCD_LIB_CMDS

#define LCD_CLEAR_DISP 0x01
#define LCD_CURSOR_HOME 0x02
#define LCD_ENTRY_MODE 0x04
#define LCD_DISPLAY_MODE 0x08
#define LCD_SHIFT_MODE 0x10
#define LCD_FUNCTION_MODE 0x20
#define LCD_CGRAM_ADDR 0x40
#define LCD_DDRAM_ADDR 0x80

#endif

#ifndef CHAR_LCD_LIB_ADDRESSES
#define CHAR_LCD_LIB_ADDRESSES

#define LCD_FIRST_CHAR 0x80

#endif

#ifndef CHAR_LCD_LIB_DIMENSIONS
#define CHAR_LCD_LIB_DIMENSIONS

#define LCD_NUM_COLS 16
#define LCD_NUM_ROWS 2

#endif
